// import the express
const express = require('express')

// create express application
const app = express()

// route
// - mapping of request method and the url

// router for handling a request with
// method   = GET
// url/path = /
app.get('/', (request, response) => {
  console.log('received a GET request for /')
  response.end('Get method processed')
})

// start the express application on a required port
app.listen(4000, () => {
  console.log(`server started on port 4000`)
})
